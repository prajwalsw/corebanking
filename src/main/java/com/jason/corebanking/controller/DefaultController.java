/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.jason.corebanking.controller;

import com.jason.corebanking.dao.AccountDao;
import com.jason.corebanking.dao.impl.AccountDaoImpl;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Prajwal
 */
public class DefaultController extends HttpServlet {
    AccountDao accountDao=new AccountDaoImpl();
    @Override
    protected void doGet(HttpServletRequest request,HttpServletResponse response)throws ServletException , IOException{
        request.setAttribute("accDao",accountDao.getAll());
        RequestDispatcher rd= request.getRequestDispatcher("/WEB_INF/index.jsp"); 
        rd.forward(request, response); 
    }
    
}
