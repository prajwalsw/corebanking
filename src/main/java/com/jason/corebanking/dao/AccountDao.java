/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.jason.corebanking.dao;

import com.jason.corebanking.entity.Account;
import java.util.List;

/**
 *
 * @author Prajwal
 */
public interface AccountDao {
    List<Account> getAll();
}
