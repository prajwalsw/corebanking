/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.jason.corebanking.dao.impl;

import com.jason.corebanking.dao.AccountDao;
import com.jason.corebanking.entity.Account;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Prajwal
 */
public class AccountDaoImpl implements AccountDao{

    @Override
    public List<Account> getAll() {
        List<Account> accounts= new ArrayList<>();
        accounts.add(new Account(1,"Normal Savings","Normal Savings",2.5,0,100,true));
        accounts.add(new Account(2,"Super Savings","Super Savings",4.5,10000,100,false));
        return accounts;
    }
    
}
