
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %> 




<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body
        <table border="1">
            <tr>
                <th>Id</th>
                <th>Name</th>
                <th>Interest</th>
                <th>Minimum Balance</th>
                <th>Status</th>
            </tr>
            <c:forEach var="account" items="${requestScope.accDao}">
                <tr>
                    <td>${account.id}</td>
                    <td>${account.name}</td
                    <td>${account.interest}</td>
                    <td>${account.minimumBalance}</td>
                    <td>
                        <c:choose >
                            <c:when test="${account.status}">
                                Active
                            </c:when>
                            <c:otherwise>
                                InActive
                            </c:otherwise>
                        </c:choose>
                        </td>
                </tr>
            </c:forEach>

        </table>
    </body>
</html>
